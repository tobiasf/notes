# -*- coding: utf-8 -*-

import os
import datetime
import commonmark
import json

from session import session
from dateutil.relativedelta import relativedelta
from bottle import route, request, redirect, run, static_file
from genshi.template import TemplateLoader
from model.birthdays import delete_birthday, get_birthdays_dict, get_birthdays, get_birthday, add_birthday
from model.bookmarks import get_bookmark, get_bookmarks, delete_bookmark, save_bookmark
from model.tasks import get_tasks, get_top_tasks, count_tasks, get_task, get_priorities, \
    get_interval_types, add_task, delete_task, set_done, get_completed_tasks
from model.tutorials import delete_tutorial, save_folder, get_folders, save_tutorial, get_tutorial, \
                            get_tutorials, filter_tutorials
from model.misc import get_user_id


DEVELOPMENT_VERSION = False
TITLE = 'Notes'

loader = TemplateLoader(
    os.path.join(os.path.dirname(__file__), 'templates'),
    auto_reload=True
)


def print_params():
    print('\nParams:')
    for k, v in request.params.items():
        print(f'\t{k}: {v}')


def render(template_name, template_vars={}, jsonify=True, shrink=True):
    def shrink_html(html):
        n = []
        for line in html.split('\n'):
            n.append(line.strip())
        return ''.join(n)

    template_vars['request'] = request
    template_vars['session'] = session
    template_vars['development_version'] = DEVELOPMENT_VERSION
    template_vars['title'] = TITLE

    tmpl = loader.load(template_name)
    tmpl = tmpl.generate(**template_vars).render('html')
    if shrink and not DEVELOPMENT_VERSION:
        tmpl = shrink_html(tmpl)

    if not jsonify:
        return tmpl
    return json.dumps({'html': tmpl, 'timestamp': request.params['timestamp']})


def _calendar_params():
    params = {}
    if request.params.get('calendar_date', None):
        params['calendar_date'] = datetime.datetime.strptime(request.params['calendar_date'], '%Y-%m-%d').date()
    else:
        params['calendar_date'] = datetime.date.today()

    first_of_month = params['calendar_date'].replace(day=1)
    # set the start date to previous monday if its not monday
    start_date = first_of_month - relativedelta(days=first_of_month.weekday())
    end_date = start_date + relativedelta(days=34)
    number_of_days = ((first_of_month + relativedelta(months=1)) - first_of_month).days
    if end_date < first_of_month.replace(day=number_of_days):
        end_date = end_date + relativedelta(days=7)

    tasks = get_tasks(start_date, end_date)
    params['tasks'] = {}
    for task in tasks:
        if not task['date'] in params['tasks']:
            params['tasks'][task['date']] = []
        params['tasks'][task['date']].append(task)

    params['dates'] = [start_date]
    while params['dates'][-1:][0] < end_date:
        params['dates'].append((params['dates'][-1:][0] + relativedelta(days=1)))

    params['birthdays'] = get_birthdays_dict()
    params['today'] = datetime.date.today()
    return params


@route('/favicon.ico', method='GET')
def _favicon():
    return static_file('favicon.ico', root='./images/')


@route('/', method='GET')
def _login_template():
    if session.is_valid():
        return _main_template()

    user_id = get_user_id('tobi')
    session.new('tobi', user_id)
    return _main_template()


@route('/main', method='GET')
def _main_template(params={}):
    params.update({'sideboxes': [
        {
            'headline': 'Birthdays',
            'headlines': ['Name', 'Date', 'Age', 'Days'],
            'keys': ['name', 'birthday', 'age', 'days'],
            'bottom_links': [('/birthdays/edit', 'add'), ],
            'rows': get_birthdays(31, '%d. %b'),
        }, {
            'headline': 'Tasks',
            'headlines': ['Name'],
            'keys': ['name'],
            'bottom_links': [('/tasks/edit', 'add'), ],
            'rows': get_top_tasks(10),
            'quantity': count_tasks(),
        }
    ]})
    params.update(_calendar_params())
    return render('index.html', params, jsonify=False)


@route('/logout', method='GET')
def _logout():
    session.destroy()
    redirect('/')


@route('/calendar', method="post")
def _calendar():
    session.get()
    return render('calendar.html', _calendar_params())


@route('/tasks/edit', method="post")
def _edit_task(task=None):
    session.get()
    params = {'errors': {}}
    if task:
        params['task'] = task
    elif 'task_id' in request.params:
        params['task'] = get_task(int(request.params['task_id']))
    else:
        params['task'] = get_task(int(request.params.get('task_id', -1)))
        params['task']['date'] = request.params.get('calendar_date', datetime.date.today())

    params['priorities'] = get_priorities()
    params['interval_types'] = get_interval_types()
    return render('edit_task.html', params)


@route('/tasks', method="post")
def _tasks():
    if request.params.get('calendar_date', None):
        return _calendar()
    params = {
        'tasks': get_tasks(tasks_only=True),
        'today': datetime.date.today(),
    }
    return render('tasks.html', params)


@route('/tasks/save_task', method="post")
def _save_task():
    def get_interval():
        interval_type = int(request.params['interval_type'])
        if interval_type == 1:  # once
            return None
        elif interval_type == 2:  # weekly
            return int(request.params['day'])
        elif interval_type == 3:  # monthly
            return int(request.params['dom'])
        elif interval_type == 4:  # yearly
            return int(request.params['dom'])
        elif interval_type == 5:  # every x days
            return int(request.params['days'])

    params = {}

    task = {
        'id': int(request.params.get('id', -1)),
        'user': None,
        'task': request.params['task'],
        'name': request.params['name'] if request.params['name'] != '' else None,
        'interval_type': int(request.params['interval_type']),
        'interval': get_interval(),
        'date': request.params['date'],
        'end_date': request.params['end_date'],
        'priority': int(request.params['priority']),
        'time': request.params['time'] if request.params['time'] not in ['HH:MM', ''] else None,
        'month_of_year': int(request.params['moy']) if int(request.params['interval_type']) == 4 else None,
        'parent': None,
        'url': request.params['url'],
    }

    params['errors'] = add_task(task)
    if params['errors']:
        return _edit_task(task)
    delete_task(int(request.params['old_task_id']))
    return _tasks()


@route('/tasks/complete', method="post")
def _complete_task():
    set_done(request.params.get('task_id', -1))
    return _tasks()


@route('/tasks/completed', method="post")
def _completed_tasks():
    params = {
        'tasks': get_completed_tasks(),
        'today': datetime.date.today(),
    }
    return render('tasks.html', params)


@route('/tasks/delete', method="post")
def _delete_tasks():
    delete_task(request.params.get('task_id', -1))
    return _tasks()


@route('/birthdays', method="post")
def _birthdays():
    return render('birthdays.html', {'birthdays': get_birthdays()})


@route('/birthdays/edit', method="post")
def _edit_birthday():
    params = {'birthday': {}}
    if 'birthday_id' in request.params:
        params = {'birthday': get_birthday(request.params['birthday_id'])}
    return render('edit_birthday.html', params)


@route('/birthdays/save', method="post")
def _save_birthday():
    if 'birthday_id' in request.params:
        delete_birthday(request.params['birthday_id'])
    add_birthday(request.params['name'], request.params['date'])
    return _birthdays()


@route('/birthdays/delete', method="post")
def _birthday_delete():
    delete_birthday(request.params['birthday_id'])
    return _birthdays()


@route('/bookmarks', method="post")
def _bookmarks():
    params = {'bookmarks': get_bookmarks()}
    return render('bookmarks.html', params)


@route('/bookmarks/edit', method="post")
def _edit_bookmark():
    params = {'bookmark': {}}
    if 'bookmark_id' in request.params:
        params = {'bookmark': get_bookmark(request.params['bookmark_id'])}
    return render('edit_bookmark.html', params)


@route('/bookmarks/save', method="post")
def _save_bookmark():
    if 'bookmark_id' in request.params:
        delete_bookmark(request.params['bookmark_id'])
    save_bookmark(request.params['name'], request.params['url'])
    return _bookmarks()


@route('/bookmarks/delete', method="post")
def _delete_bookmark():
    delete_bookmark(request.params['bookmark_id'])
    return _bookmarks()


@route('/tutorials', method="post")
def _tutorials():
    if 'id' in request.params:
        tutorial = get_tutorial(request.params['id'])
        tutorial['description'] = commonmark.commonmark(tutorial['description'])
        params = {'tutorial': tutorial}
        return render('tutorial.html', params, shrink=False)
    params = {
        'folders': get_folders(),
        'tutorials': get_tutorials()
    }
    return render('tutorials.html', params, shrink=False)


@route('/tutorials/edit', method="post")
def _edit_tutorial():
    params = {
        'folders': get_folders(),
        'tutorial': None,
    }
    if 'id' in request.params:
        params['tutorial'] = get_tutorial(request.params['id'])
    return render('edit_tutorial.html', params, shrink=False)


@route('/tutorials/save', method="post")
def _save_tutorial():
    if 'id' in request.params:
        delete_tutorial(request.params['id'])
        del(request.params['id'])
    save_tutorial(request.params['name'], request.params['description'],
                  request.params['folder'] if request.params['folder'] != '' else None)
    return _tutorials()


@route('/tutorials/filter', method="post")
def _filter_tutorials():
    tutorials = filter_tutorials(request.params.get('filter_string', ''), request.params.get('folder_id', None))
    # TODO: sometimes list, sometimes dict? who wrote this??
    if isinstance(tutorials, list) and len(tutorials) == 1:  # single tutorial found
        tutorial = tutorials[0]
        tutorial['description'] = commonmark.commonmark(tutorial['description'])
        params = {'tutorial': tutorial}
        return render('tutorial.html', params, shrink=False)
    if len(tutorials.keys()) == 1 and len(list(tutorials.values())[0]) == 1:
        params = {'tutorial': list(tutorials.values())[0][0]}
        params['tutorial']['description'] = commonmark.commonmark(params['tutorial']['description'])
        return render('tutorial.html', params, shrink=False)

    params = {
        'tutorials': tutorials,
        'folders': get_folders(),
    }
    return render('tutorials.html', params, shrink=False)


@route('/tutorials/add_folder', method="post")
def _add_folder():
    if save_folder(request.params['folder_name']):
        params = {
            'folders': get_folders()
        }
        return render('ajax/folders.html', params)
    return 0


@route('/tutorials/delete', method="post")
def _delete():
    delete_tutorial(request.params['id'])
    del(request.params['id'])
    return _tutorials()


# static files
@route('/:folder/:filename#.*#', method='GET')
def _static_file(folder, filename):
    return static_file(filename, root=f"./{folder}/")


if __name__ == "__main__":
    run(reloader=True, port=8666)
