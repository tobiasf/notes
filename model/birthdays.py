from database import db
from session import session


def get_birthday(birthday_id):
    return db.fetchone('''SELECT * from birthdays WHERE id = ?''', [birthday_id, ])


def get_birthdays_dict():
    data = {}
    result = db.fetchall('SELECT * FROM birthdays')
    for line in result:
        date = f"{line['birthday'].month}-{line['birthday'].day}"
        if date not in data:
            data[date] = []
        data[date].append(line)
    return data


def get_birthdays(next_days=None, format_string=None):
    import datetime
    from operator import itemgetter
    d = datetime.date
    current_year = d.today().year

    data = []
    if next_days is None:
        next_days = 365

    result = db.fetchall('SELECT * FROM birthdays ORDER BY birthday ASC')
    for line in result:
        enhanced_line = dict(line)
        enhanced_line['age'] = current_year - enhanced_line['birthday'].year

        birthday = line['birthday'].replace(year=current_year)

        if birthday < d.today():
            birthday = line['birthday'].replace(year=current_year+1)
            enhanced_line['age'] += 1

        enhanced_line['days'] = (birthday - d.today()).days
        if format_string is not None:
            enhanced_line['birthday'] = enhanced_line['birthday'].strftime(format_string)

        if enhanced_line['days'] <= next_days:
            data.append(enhanced_line)
    data.sort(key=itemgetter('days'))
    return data


def add_birthday(name, birthday, user_id=None):
    db.execute('''INSERT INTO birthdays (name, birthday, "user") VALUES (?, ?, ?)''', [name, birthday, user_id])


def delete_birthday(birthday_id):
    db.execute('''DELETE FROM birthdays WHERE id = ? AND ("user" = ? OR "user" IS NULL)''', [birthday_id, session.user_id])
