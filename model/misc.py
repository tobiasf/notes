import os

from database import db

DEBUG = 0
development_version = os.uname()[1] == 'sg31g2b'


def get_user_id(username):
    return db.fetchone('''SELECT id FROM user WHERE username = ?''', [username])['id']
