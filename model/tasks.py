from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from copy import deepcopy
from database import db
from session import session


def count_tasks():
    return db.fetchone('''SELECT COUNT(*) AS quantity from tasks WHERE task = 1 AND done = 0 AND (user = ? OR user IS NULL)''', [session.user_id])['quantity']


def set_done(id, done=True):
    db.execute('''UPDATE tasks SET done = ? WHERE id = ? AND (user = ? OR user IS NULL)''', [done, id, session.user_id])


def delete_task(id):
    # TODO: delete 1 or all?
    db.execute('''DELETE FROM tasks WHERE id = (SELECT parent FROM tasks WHERE id = ? AND (user = ? OR user IS NULL))''', [id,session.user_id])
    db.execute('''DELETE FROM tasks WHERE id = ? AND (user = ? OR user IS NULL)''',[id,session.user_id])


def get_interval_types():
    result = db.fetchall('''SELECT * FROM interval ORDER BY id''')
    data = {}
    for line in result:
        data[line['id']] = line['name']
    return {'result':result, 'dict':data}


def get_priorities():
    result = db.fetchall('''SELECT * FROM priority ORDER BY id''')
    data = {}
    for line in result:
        data[line['id']] = line['name']
    return {'result': result, 'dict': data}


def get_task(id):
    if id < 1:
        return {
            'id': -1,
            'name': '',
            'interval_type': 1,
            'date': datetime.today().date(),
            'end_date': datetime.today().date(),
            'interval': 1,
            'priority': 2,
            'time': 'HH:MM',
            'user': None,
            'month_of_year': 1,
            'url': '',
        }
    return db.fetchone('''SELECT * FROM tasks WHERE id = ? AND (user = ? OR user IS NULL)''', [id, session.user_id])


def get_top_tasks(limit):
    return db.fetchall('''SELECT * FROM tasks WHERE task = 1 AND done = 0 AND (user = ? OR user IS NULL) ORDER BY priority DESC, date ASC LIMIT ?''', [session.user_id, limit])


def get_tasks(start_date = None, end_date = None, tasks_only = False):
    params = [session.user_id]
    query = '''SELECT * FROM tasks WHERE done = 0 AND (user = ? OR user IS NULL)'''
    if start_date != None and end_date != None:
        query += ' AND date >= ? AND date <= ? '
        params.append(start_date)
        params.append(end_date)
    if tasks_only:
        query += ' AND task = 1 ORDER BY priority DESC, date ASC, time ASC, name ASC'
    else:
        query += 'ORDER BY date ASC, time ASC, priority DESC, name ASC'
    return db.fetchall(query, params)


def get_completed_tasks():
    return db.fetchall('''SELECT * FROM tasks WHERE done = 1 AND (user = ? OR user IS NULL) ORDER BY date DESC''', [session.user_id])


def add_task(task_obj):
    def _is_legal_date(date_obj, interval_type, interval, month_of_year):
        """
            Checks whether the date_obj is a legal date concerning the parameters
        """
        if interval_type in [1,5]:
            return True
        elif interval_type == 2: #weekly
            return interval == date_obj.weekday()
        elif interval_type == 3: #monthly
            return interval == date_obj.day
        elif interval_type == 4: #yearly
            return interval == date_obj.day and date_obj.month == month_of_year
        raise ValueError('interval_type must be in [1,..,5]')

    def _get_next_legal_date(date_obj, interval_type, interval, month_of_year):
        """
            This function will never return the input date! It will always return a date in the future.
            input example:
            date_obj=1981-10-18, which was a sunday (6), interval_type = 2 (weekly), interval = 1
            would return 1981-10-19, the next monday

            interval = 6 (sunday) would return 1981-10-25 the next sunday
        """

        assert interval_type in [2,3,4,5], 'interval_type = %s not in [2,3,4,5]' %interval_type

        # weekly
        if interval_type == 2:
            """Monday is 0 and Sunday is 6 as in date.weekday()"""
            day_diff = (interval - date_obj.weekday() + 7) % 7
            if day_diff == 0: day_diff = 7
            return date_obj + timedelta(days = day_diff)

        #monthly
        elif interval_type == 3:
            if date_obj.timetuple()[2] < interval:
                return date_obj.replace(day = interval)
            return date_obj.replace(day = interval) + relativedelta(months = 1)

        #yearly
        elif interval_type == 4:
            next_possible_date = date_obj.replace(month = int(month_of_year), day = int(interval))
            if next_possible_date > date_obj:
                return next_possible_date
            return next_possible_date.replace(year = next_possible_date.year + 1)

        #every x days
        elif interval_type == 5:
            return date_obj + timedelta(days = interval)

    def parse_bool(b):
        if b == None or b == 'None':
            return None
        if b in [True, 'True', 'true', 'TRUE', '1', 1]:
            return 1
        if b in [False, 'False', 'false', 'FALSE', '0', 0]:
            return 0
        raise ValueError('%s not parseable to booelan' %b)


    def insert_task(task):
        """
            Inserts one line into table task. When no id is given it will take one from the sequence.
        """
        task['task'] = parse_bool(task['task'])
        params = ['id', 'name', 'priority', 'interval_type', 'interval', 'user', 'date', 'end_date', 'parent', 'month_of_year', 'task', 'time', 'url']
        if task['id'] < 1:
            params = params[1:]
        query = ','.join(params)
        db.execute('''INSERT INTO tasks(%s) VALUES (%s)'''%(query,','.join(['?' for i in range(len(params))])), [task[key] for key in params])

    error_messages = []

    try:
        task_obj['date'] = datetime.strptime(task_obj['date'], '%Y-%m-%d').date()
        task_obj['end_date'] = datetime.strptime(task_obj['end_date'], '%Y-%m-%d').date() if task_obj['end_date'] != None else None

        if task_obj['interval_type'] == 1:
            task_obj['end_date'] = task_obj['date']
            insert_task(task_obj)

        else:
            if not _is_legal_date(task_obj['date'], task_obj['interval_type'], task_obj['interval'], task_obj['month_of_year']):
                task_obj['date'] = _get_next_legal_date(task_obj['date'], task_obj['interval_type'], task_obj['interval'], task_obj['month_of_year'])

            insert_task(task_obj) #adds a new id to the task_obj!!
            current_date = _get_next_legal_date(task_obj['date'], task_obj['interval_type'], task_obj['interval'], task_obj['month_of_year'])

            while current_date <= task_obj['end_date']:
                new_task_obj = deepcopy(task_obj)
                new_task_obj['parent'] = task_obj['id'] #used here
                new_task_obj['date'] = current_date
                insert_task(new_task_obj)
                current_date = _get_next_legal_date(current_date, task_obj['interval_type'], task_obj['interval'], task_obj['month_of_year'])

    except ValueError as e:
        error_messages.append(e)
    return error_messages
