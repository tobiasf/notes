from database import db
from session import session

def get_top_bookmarks(top=5):
    return db.fetchall('''SELECT * FROM bookmarks WHERE "user" = ? ORDER BY counter DESC, name ASC LIMIT ?''', [session.user_id, top])

def get_bookmark(id):
    return db.fetchone('''SELECT * FROM bookmarks WHERE id = ? AND "user" = ?''', [id, session.user_id])

def get_bookmarks():
    return db.fetchall('''SELECT * FROM bookmarks WHERE "user" = ? ORDER BY name''', [session.user_id])

def save_bookmark(name, url):
    db.execute('''INSERT INTO bookmarks (name, url, "user") VALUES (?, ?, ?)''', [name, url, session.user_id])

def delete_bookmark(id):
    db.execute('''DELETE FROM bookmarks WHERE id = ? AND "user" = ?''', [id,session.user_id])

def reset_statistics():
    db.execute('''UPDATE bookmarks SET counter = 0 WHERE "user" = ?''', [session.user_id])
