from database import db
from session import session


def order_by_initial(tutorials_input):
    tutorials = {}
    for tutorial in tutorials_input:
        initial = tutorial['name'][0].upper()
        if initial not in tutorials:
            tutorials[initial] = []
        tutorials[initial].append(tutorial)
    return tutorials


def save_folder(folder_name):
    db.execute('''INSERT INTO tutorials_folder (name, user) VALUES (?, ?)''', [folder_name, session.user_id])


def get_folders():
    return db.fetchall('''SELECT * FROM tutorials_folder WHERE user = ? ORDER BY name''', [session.user_id])


def get_top_folders(count=5):
    return db.fetchall('''
        SELECT
            f.id,
            f.name,
            COUNT(*)
        FROM
            tutorials_folder f,
            tutorials t
        WHERE
            f.id = t.folder
        GROUP BY
            f.id,
            f.name
        ORDER BY COUNT DESC LIMIT ?''', [count])


def get_tutorial(id):
    return db.fetchone('''SELECT * FROM tutorials WHERE id = ? AND user = ?''', [id, session.user_id])


def get_tutorials():
    result = db.fetchall('''SELECT id, name, folder FROM tutorials WHERE user = ? ORDER BY lower(name)''', [session.user_id])
    return order_by_initial(result)


def save_tutorial(name, desc, folder=None):
    assert name, "No Name given for tutorial"
    db.execute('''INSERT INTO tutorials (name, description, folder, user) VALUES (?, ?, ?, ?)''', [name, desc, folder, session.user_id])


def delete_tutorial(id):
    db.execute('''DELETE FROM tutorials WHERE id = ? AND user = ?''', [id, session.user_id])


def filter_tutorials(string, folder_id=None):
    # if only 1 char is provided
    if len(string) == 1:
        result = db.fetchall('''SELECT * FROM TUTORIALS WHERE LOWER(NAME) LIKE ?''', [string+'%'])
        if len(result) == 1:
            result = [result[0]]
        return order_by_initial(result)

    # look for exact match
    result = db.fetchall('''SELECT * FROM TUTORIALS WHERE LOWER(NAME) = ?''', [string])
    if result:
        return result

    def all_perms(lst):
        lst = list(lst)
        if len(lst) <= 1:
            yield lst
        else:
            for perm in all_perms(lst[1:]):
                for i in range(len(perm)+1):
                    yield perm[:i] + lst[0:1] + perm[i:]

    # omg, who writes this code??
    search_strings = ['%s%s%s' %('%', '%'.join(p), '%') for p in all_perms(map(lambda x: x.lower(), string.split()))]
    base_stmt = """SELECT * FROM TUTORIALS WHERE LOWER(NAME) LIKE ? OR LOWER(DESCRIPTION) LIKE ?"""
    stmt = ''
    params = []
    for perm in search_strings:
        stmt += base_stmt
        params.append(perm)
        params.append(perm)
        if perm != search_strings[-1:][0]:
            stmt += '\nUNION\n'

    if folder_id:
        stmt = "SELECT * FROM (" + stmt + ") AS foo WHERE folder = ?"
        params.append(int(folder_id))
    result = db.fetchall(stmt, params)
    if len(result) == 1:
        tutorial = result[0]
        result = [tutorial]
    return order_by_initial(result)
