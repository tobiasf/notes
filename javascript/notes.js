var last_request = null;
//var session_id = null;

function session_time(params){
    if (params === undefined){
        var params = {};
    }
    var d = new Date();
    last_request = d.getTime();
    //params['session_id'] = session_id;
    params['timestamp'] = last_request;
    return params
}


/**
    Returns a standard key, value dict.
*/
function serializedArray_to_dict(serialized_array){
    var params = {}
    for (var i=0; i< serialized_array.length; i++){
        params[serialized_array[i].name] = serialized_array[i].value;
    }
    //TODO: include alert
    //alert('Error: duplicate items in serializedArray');
    return params;
}


function logout(){
    session_id = 0;
    window.location.href = '/';
    return false;
}

function load_content(href, params){
    $('#tooltip').hide();
    $('#main_content').empty();
    $('#loading').show();
    $.ajax({
        'type': 'POST',
        'url': href,
        'dataType': 'json',
        'data': session_time(params),
        'success': function(response){
            if (parseInt(response['timestamp']) >= last_request){
                $('#loading').hide();
                $('#main_content').html(response.html);
                bind_load_content_to_links('main_content');
                onload();
            }
        },
        'error': function(response){
            $('#loading').hide();
            $('#main_content').html(response.responseText);
        }
    });
    return false;
}

/**
Triggers the load content function when a link is clicked
*/
function bind_load_content_to_links(id){
    $('#' + id).find('a.ajax').click(function(){
        var iframe_id = $('textarea.tutorial_description').attr('id');
        if (iframe_id){
            $('textarea.tutorial_description').html($('#' + iframe_id + '_ifr').contents().find('#tinymce').html());
        }
        var params = {};
        if ($(this).attr('rel') !== ""){
            params = serializedArray_to_dict($('#' + $(this).attr('rel')).serializeArray());
        }
        return load_content($(this).attr('href'), params);
    });
}

//Filters tutorials, triggered when key is pressed in search field
function filter_tutorials(evt){
    if (!evt) var evt = window.event;
    var keyCode = evt.keyCode || evt.charCode;
    if (keyCode == 46 || keyCode == 32 || keyCode == 8 || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (keyCode >= 48 && keyCode <= 57) || (keyCode == 222) || (keyCode == 186) || (keyCode == 219) || (keyCode == 189)){
        load_content('/tutorials/filter', session_time({
            'filter_string': $('#search').val()
        }));
    }
}

function check_element(myself){
    if (myself.find('input').attr('checked')){
        $('.highlighted input:checked').attr('checked', false);
        $('.highlighted').removeClass('highlighted');
    }else{
        $('.highlighted input:checked').attr('checked', false);
        $('.highlighted').removeClass('highlighted');
        myself.find('input').attr('checked', 'checked');
        myself.addClass('highlighted');
    }
}

function add_folder(){
    $.get('/tutorials/add_folder',session_time({'folder_name':$('#folder_name').val()}),function(data){
        if (data !== '0'){
            $('#folders').html(data);
            $('#add_folder_div').hide();
        }else{
            alert('Folder exists!');
        }
    });
    return false;
}

function set_done(myself){
    var input_elem = myself.children('.done').children('input');
    if (input_elem.attr('checked')){
        input_elem.attr('checked',false);
        $.get('/tasks/set_done', session_time({'id':input_elem.val(),'done':false}));
    }else{
        input_elem.attr('checked','checked');
        $.get('/tasks/set_done', session_time({'id':input_elem.val(),'done':true}));
    }
}

function check_input_error(myself){
    var label = $(myself).parent().prev();
    if (label.hasClass('error')){
        label.removeClass('error');
        $(myself).val('');
    }
}

function hide_add_task(){
    $('#add_task').hide();
    return false;
}

function show_add_task(){
    $('.add_item').hide();
    $('#add_task_name').val('');
    $('#input_add_task_date').val('');
    $('#add_task').show();
    return false;
}

function hide_add_birthday(){
    $('#add_birthday').hide();
    return false;
}

function show_add_birthday(){
    $('.add_item').hide();
    $('#add_birthday_name').val('');
    $('#input_add_birthday_date').val('');
    $('#add_birthday').show();
    return false;
}


function submit_task_by_enter(e){
    if (e.keyCode == 13){
        $('#save').click();
    }
}


function select_interval(interval_id, popup){
    $('.interval').hide();
    $('#add_task_dates').show();
    if (interval_id === 1){
        $('#add_task_end_date').hide();
    }else if(interval_id === 2){
        $('#add_task_day').show();
        $('#add_task_end_date').show();
        $('#add_task_date_label').text('Initial Date:');
    }else if(interval_id === 3){
        $('#add_task_date_label').text('Initial Date:');
        $('#add_task_end_date').show();
        $('#add_task_dom').show();
    }else if(interval_id === 4){
        $('#add_task_date_label').text('Initial Date:');
        $('#add_task_end_date').show();
        $('#add_task_dom').show();
        $('#add_task_moy').show();
    }else if(interval_id === 5){
        $('#add_task_date_label').text('Initial Date:');
        $('#add_task_end_date').show();
        $('#add_task_days').show();
    }
    return false;
}


function update_selected_style(){
    $('input:radio').next().removeClass('focussed');
    $('input:radio:checked').next().addClass('focussed');
}


function onload(){
    update_selected_style();
    $('input:radio').focus(update_selected_style);
    $('input:radio').blur(update_selected_style);
    $('input:radio').change(update_selected_style);

    $("#input_add_task_date").date_input();
    $("#input_add_task_end_date").date_input();
    $("#input_add_birthday_date").date_input();

    $('div.post table').tablesorter();
    $('#search').focus();

    $("table.calendar_table").find('td.tooltip').tooltip({
        bodyHandler: function() {
            return $($(this).attr("href")).html();
        },
        showURL: false,
        track: true,
        top: 0,
        left: -130
    });
}

/** Added function for machine readable date */
$(document).ready(function() {
    $.extend(DateInput.DEFAULT_OPTS, {
        stringToDate: function(string) {
            var matches;
            if (matches = string.match(/^(\d{4,4})-(\d{2,2})-(\d{2,2})$/)) {
                return new Date(matches[1], matches[2] - 1, matches[3]);
            } else {
                return null;
            };
        },

        dateToString: function(date) {
            var month = (date.getMonth() + 1).toString();
            var dom = date.getDate().toString();
            if (month.length == 1) month = "0" + month;
            if (dom.length == 1) dom = "0" + dom;
            return date.getFullYear() + "-" + month + "-" + dom;
        }
    });

    bind_load_content_to_links('body');
    $('#body').fadeIn('slow', function(){
        onload();
    });
})
