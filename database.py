#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3
import threading
import settings


class Connection(object):
    def __enter__(self):
        def dict_factory(cursor, row):
            d = {}
            for idx, col in enumerate(cursor.description):
                d[col[0]] = row[idx]
            return d

        self._con = sqlite3.connect(
            settings.DB,
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        self._con.isolation_level = None  # autocommit
        self._con.row_factory = dict_factory
        self._cursor = self._con.cursor()
        return self._cursor

    def __exit__(self, *args):
        self._cursor.close()
        self._con.close()


class Db(threading.local):
    def execute(self, *args, **kwargs):
        with Connection() as cursor:
            return cursor.execute(*args, **kwargs)

    def executemany(self, *args, **kwargs):
        with Connection() as cursor:
            return cursor.executemany(*args, **kwargs)

    def fetchone(self, *args, **kwargs):
        with Connection() as cursor:
            cursor.execute(*args, **kwargs)
            return cursor.fetchone()

    def fetchmany(self, *args, **kwargs):
        with Connection() as cursor:
            cursor.execute(*args, **kwargs)
            return cursor.fetchmany(*args, **kwargs)

    def fetchall(self, *args, **kwargs):
        with Connection() as cursor:
            cursor.execute(*args, **kwargs)
            return cursor.fetchall()


db = Db()
