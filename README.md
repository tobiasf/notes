# README #

### What is this repository for? ###

* Personal Notes, Tutorials, Birthdays all in a nice 10year old UI
* Uses SQLite (save in Dropbox for "cloud" experience).

### How do I get set up? ###

* Clone it
* virtualenv env; source env/bin/activate; pip install -r requirements.txt
* Edit settings.py and adjust the path to your SQLite DB
* python ./tools/create_sqlite_db.py
* python notes.py
* http://localhost:8666
