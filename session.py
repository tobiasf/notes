import random
from bottle import request, response
from model.misc import get_user_id
from time import time


class SessionNotFound(Exception):
    def __init__(self, session_id):
        self.value = 'Session not found: {session_id}. Logout and -in again.'

    def __str__(self):
        return repr(self.value)


class SingleSession():
    id = None
    username = None
    user_id = None
    timestamp = None

    def __init__(self, username, user_id):
        def dict_factory(cursor, row):
            d = {}
            for idx, col in enumerate(cursor.description):
                d[col[0]] = row[idx]
            return d

        self.username = username
        self.user_id = user_id
        self.id = self._generate_session_id()
        self.timestamp = time()

    def _generate_alphabet(self):
        return \
            [str(i) for i in range(0, 10)] + \
            [chr(i) for i in range(65, 91)] + \
            [chr(i) for i in range(97, 123)]

    def _generate_session_id(self):
        s_id = []
        alphabet = self._generate_alphabet()
        for i in range(32):
            s_id.append(random.choice(alphabet))
        return ''.join(s_id)

    def __str__(self):
        return f'\nsession_id: {self.id}\nusername: {self.username}'


class SessionContainer():
    sessions = None

    def __init__(self):
        self.sessions = {}

    @property
    def id(self):
        return self.get().id

    @property
    def username(self):
        return self.get().username

    @property
    def user_id(self):
        return self.get().user_id

    @property
    def timestamp(self):
        return self.get().timestamp

    def new(self, username, user_id):
        session = SingleSession(username, user_id)
        while (session.id in self.sessions):
            session = SingleSession(username, user_id)

        # when creating a new session removing old sessions for this user
        for session_id, session in list(self.sessions.items()):
            if session.username == username:
                del(self.sessions[session_id])

        self.sessions[session.id] = session
        max_age = 3600*48  # 1hour *48 = 2days
        response.set_cookie('sid', session.id, max_age=max_age)
        return session.id

    def is_valid(self):  # TODO: remove auth
        return True

    def get(self):
        """
        Creates a new session if not exists already.
        """
        session_id = request.get_cookie('sid')
        if not session_id or session_id not in self.sessions:
            session_id = self.new('tobi', get_user_id('tobi'))
        return self.sessions[session_id]

    def destroy(self):
        session_id = request.get_cookie('sid')
        if session_id not in self.sessions:
            raise SessionNotFound(session_id)
        del(self.sessions[session_id])
        return session_id

    def __str__(self):
        s = [f'\nSessions ({len(self.sessions.keys())})']
        s.append('\n---------------------------------------------')
        for session in self.sessions.values():
            s.append(str(session))
            s.append('\n---------------------------------------------')
        return ''.join(s)


session = SessionContainer()
