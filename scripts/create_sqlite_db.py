#!/usr/bin/python
# -*- coding: utf-8 -*-

import sqlite3
import os
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
import .settings


# os.remove(settings.DB)
con = sqlite3.connect(settings.DB)
cur = con.cursor()
queries = [
    '''
    CREATE TABLE user (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username TEXT UNIQUE NOT NULL,
        password TEXT NOT NULL
    )''',

    '''
    CREATE TABLE birthdays (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT UNIQUE NOT NULL,
        birthday DATE NOT NULL,
        user INT CONSTRAINT user_exists REFERENCES user(id) ON DELETE RESTRICT
    )''',
    '''
    CREATE TABLE bookmarks (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        url TEXT NOT NULL,
        name TEXT NOT NULL,
        user INT CONSTRAINT user_exists REFERENCES user(id) ON DELETE RESTRICT,
        counter INT DEFAULT 0
    )''',
    '''
    CREATE TABLE interval (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT UNIQUE NOT NULL
    )''',
    '''
    CREATE TABLE priority (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT UNIQUE NOT NULL
    )''',
    '''
    CREATE TABLE tasks (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL,
        priority INT CONSTRAINT priority_exists REFERENCES priority(id) ON DELETE RESTRICT,
        interval_type INT CONSTRAINT interval_type_exists REFERENCES interval(id) ON DELETE RESTRICT,
        user INT CONSTRAINT user_exists REFERENCES user(id) ON DELETE RESTRICT,
        interval INT,
        date DATE NOT NULL,
        end_date DATE,
        parent INT CONSTRAINT parent_exists REFERENCES task(id) ON DELETE RESTRICT,
        month_of_year INT,
        time TEXT,
        task BOOLEAN DEFAULT 0 NOT NULL,
        done BOOLEAN DEFAULT 0 NOT NULL
    )''',
    '''
    CREATE TABLE tutorials_folder (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT UNIQUE NOT NULL,
        user INT CONSTRAINT user_exists REFERENCES user(id) ON DELETE RESTRICT
    )''',

    '''
    CREATE TABLE tutorials (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT UNIQUE NOT NULL,
        description TEXT,
        folder INT CONSTRAINT folder_exists REFERENCES tutorials_folder(id),
        user INT CONSTRAINT user_exists REFERENCES user(id)
    )''',
]
for query in queries:
    cur.execute(query)
